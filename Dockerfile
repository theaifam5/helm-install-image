FROM alpine

ARG HELM_VERSION
ARG KUBECTL_VERSION

RUN ALPINE_VERSION=$(cat /etc/alpine-release | cut -d '.' -f 1,2) && \
  echo http://mirror.clarkson.edu/alpine/v$ALPINE_VERSION/main >> /etc/apk/repositories && \
  echo http://mirror1.hs-esslingen.de/pub/Mirrors/alpine/v$ALPINE_VERSION/main >> /etc/apk/repositories && \
  apk add --no-cache -U wget ca-certificates openssl git glib && \
  wget -O - https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-arm64.tar.gz | tar -zxvC /tmp && \
  mv /tmp/linux-arm64/helm /tmp/linux-arm64/tiller /usr/bin/ && \
  wget https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/arm64/kubectl && \
  chmod +x kubectl && \
  mv kubectl /usr/bin/ && \
  helm version --client && \
  kubectl version --client
